import socket
import threading
import time
from threading import Thread
from Plotter import Plotter
from EnviroReader import EnviroReader
from EnviroLCD import EnviroLCD
import logging


# Most code adapted from https://docs.python.org/3/howto/sockets.html

MSGLENGTH = 16  # Maximum byte length of message
CONNECTION_THRESHOLD = 10
PORT = 8080
DELETE_OLD_PLOTS = 356

# One-time connection only: send the data, then close the connection
class ClientHandler(Thread):

    # clientsocket
    # server
    # bme280

    def __init__(self, clientsocket, address, server, sensor, logger):
        super().__init__()
        self.clientsocket = clientsocket
        self.address = address
        self.server = server
        self.sensor = sensor
        self.logger = logger

    def run(self) -> None:
        self.logger.info("Connected to {}".format(self.address))
        connected = True
        while connected:
            rqst = self.clientsocket.recv(MSGLENGTH).decode()
            self.logger.info("Request from {}: {}".format(self.address, rqst))
            if rqst == "update":
                hum, temp, pres, _ = self.sensor.get_values()
                self.clientsocket.sendall("{0:0.1f}".format(hum).encode())
                answer = self.clientsocket.recv(MSGLENGTH).decode()
                if answer != "continue":
                    self.logger.info("Invalid continue from {}".format(self.address))
                    break
                self.clientsocket.sendall("{0:0.1f}".format(temp).encode())
                answer = self.clientsocket.recv(MSGLENGTH).decode()
                if answer != "continue":
                    self.logger.info("Invalid continue from {}".format(self.address))
                    break
                self.clientsocket.sendall("{0:0.1f}".format(pres).encode())
            elif rqst == "disconnect" or rqst == "":
                self.logger.info("Disconnecting client {}".format(self.address))
                connected = False
            else:
                self.logger.info("Unknown Request from {}".format(self.address))
                pass

        self.clientsocket.shutdown(socket.SHUT_RDWR)
        self.clientsocket.close()


class RPiTempServer:

    # serversocket

    def __init__(self):

        logging.basicConfig(
            format='%(asctime)s.%(msecs)03d %(levelname)-8s %(message)s',
            level=logging.INFO,
            datefmt='%Y-%m-%d %H:%M:%S')

        self.logger = logging.getLogger()

        self.logger.info("RPITempServer running.")

        self.serversocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        tries = 10
        while tries > 0:
            try:
                self.serversocket.bind(('', PORT))
                break
            except:
                tries -= 1
                self.logger.info("Binding Error, tries left: {}".format(tries))
                time.sleep(10)
        self.serversocket.listen(CONNECTION_THRESHOLD)
        self.sensor = EnviroReader()
        self.plotter = Plotter(DELETE_OLD_PLOTS, self.sensor, self.logger)
        self.lcd = EnviroLCD(self.sensor, self.logger)

    def server_loop(self):
        self.sensor.start()
        self.plotter.start()
        self.lcd.start()
        while True:
            (clientsocket, address) = self.serversocket.accept()
            if threading.active_count() < CONNECTION_THRESHOLD:
                ch = ClientHandler(clientsocket, address, self, self.sensor, self.logger)
                ch.start()


if __name__ == "__main__":
    server = RPiTempServer()
    server.server_loop()
