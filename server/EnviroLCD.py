# Adapted from https://github.com/pimoroni/enviroplus-python/blob/master/examples/all-in-one.py

import time
import ST7735

from PIL import Image
from PIL import ImageDraw
from PIL import ImageFont
from fonts.ttf import RobotoMedium as UserFont
from threading import Thread

class EnviroLCD(Thread):

    def __init__(self, sensor, logger, updatesec=5):
        super().__init__()

        self.logger = logger

        self.sensor = sensor
        self.aborted = False
        self.updatesec = updatesec

        # Create ST7735 LCD display class
        self.st7735 = ST7735.ST7735(
            port=0,
            cs=1,
            dc=9,
            backlight=12,
            rotation=270,
            spi_speed_hz=10000000
        )

        # Initialize display
        self.st7735.begin()
        self.backlight = Backlight(self.st7735, self.sensor, self.logger)

        self.WIDTH = self.st7735.width   # 160
        self.HEIGHT = self.st7735.height # 80

        # Set up canvas and font
        self.img = Image.new('RGB', (self.WIDTH, self.HEIGHT), color=(0, 0, 0))
        self.draw = ImageDraw.Draw(self.img)
        self.font_size = 16
        self.font = ImageFont.truetype(UserFont, self.font_size)


    def abort(self):
        self.aborted = True


    def run(self):

        # Wait for init:
        time.sleep(30)

        self.backlight.start()

        # The main loop
        while (not self.aborted):

            humidity, temperature, pressure, lux = self.sensor.get_values()

            self.display_values(temperature, humidity, pressure, lux)
            time.sleep(self.updatesec)


    # 160 x 80 Pixel gut aufteilen
    # Temp:       22.9 C°
    # Humi:       55.6 %
    # Pres:   1024.9 mbar
    # Lux:         4.0 lx

    # align='left', anchor='la'      align='right', anchor='ra'
    # (0, 0) "Temp:"                         "22.9 °C" (160, 0)
    # (0,20) "Humi:"                          "55.6 %" (160,20)
    # (0,40) "Pres:"                     "1024.9 mbar" (160,40)
    # (0,60) "Lux:"                           "4.0 lx" (160,60)

    # Displays text on the 0.96" LCD
    def display_values(self, temperature, humidity, pressure, lux):
        black = (0, 0, 0)
        white = (255, 255, 255)

        self.draw.rectangle((0, 0, self.WIDTH, self.HEIGHT), black)

        self.draw.text((0, 0), "Temp:", align='left', anchor='la', font=self.font, fill=white)
        self.draw.text((0, 20), "Humi:", align='left', anchor='la', font=self.font, fill=white)
        self.draw.text((0, 40), "Pres:", align='left', anchor='la', font=self.font, fill=white)
        self.draw.text((0, 60), "Lux :", align='left', anchor='la', font=self.font, fill=white)

        if temperature is None:
            temperature = 0.0
        if humidity is None:
            humidity = 0.0
        if pressure is None:
            pressure = 0.0
        if lux is None:
            lux = 0.0

        self.draw.text((160, 0), f"{temperature:0.1f} °C", align='right', anchor='ra', font=self.font, fill=white)
        self.draw.text((160, 20), f"{humidity:0.1f} %", align='right', anchor='ra', font=self.font, fill=white)
        self.draw.text((160, 40), f"{pressure:0.1f} mBar", align='right', anchor='ra', font=self.font, fill=white)
        self.draw.text((160, 60), f"{lux:0.1f} lx", align='right', anchor='ra', font=self.font, fill=white)

        self.st7735.display(self.img)


class Backlight(Thread):

    def __init__(self, display, sensor, logger):
        super().__init__()
        self.aborted = False
        self.display = display
        self.sensor = sensor
        self.logger = logger


    def abort(self):
        self.aborted = True


    def run(self):

        # Debounce the proximity tap:
        delay = 0.5
        last_time = 0

        self.display.set_backlight(0)

        # The main loop
        while (not self.aborted):

            time.sleep(0.5)     # busy wait, because the interrupt pin is not wired on the enviro
                                # https://github.com/pimoroni/ltr559-python/issues/6

            proximity = self.sensor.get_proximity()

            # If the proximity crosses the threshold, toggle the mode
            if proximity > 1500 and time.time() - last_time > delay:
                last_time = time.time()
                # backlight on
                self.display.set_backlight(1)
                self.logger.info("Backlight ON")

                time.sleep(10)
                # Backlight off
                self.display.set_backlight(0)
                self.logger.info("Backlight OFF")
