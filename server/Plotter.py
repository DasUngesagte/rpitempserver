from threading import Thread

import matplotlib.pyplot as plt
from datetime import datetime, timedelta
from os import path, listdir, remove
import time


class Plotter(Thread):
    save_dir = r"/home/pi/env_plots/"
    old_plots = 356
    old_plots = 356

    # [0, ..., 48] for x-axis
    time_points = list(range(0, 48))
    time_labels = ["6:00", "6:30", "7:00", "7:30",
                   "8:00", "8:30", "9:00", "9:30",
                   "10:00", "10:30", "11:00", "11:30",
                   "12:00", "12:30", "13:00", "13:30",
                   "14:00", "14:30", "15:00", "15:30",
                   "16:00", "16:30", "17:00", "17:30",
                   "18:00", "18:30", "19:00", "19:30",
                   "20:00", "20:30", "21:00", "21:30",
                   "22:00", "22:30", "23:00", "23:30",
                    "0:00", "0:30", "1:00", "1:30",
                   "2:00", "2:30", "3:00", "3:30",
                   "4:00", "4:30", "5:00", "5:30"]

    # Empty data point list, reset every 24 hours for saving:
    avg_temp_data = [0] * 48
    avg_hum_data = [0] * 48
    avg_pres_data = [0] * 48

    tmp_temp_data = []
    tmp_hum_data = []
    tmp_pres_data = []

    aborted = False

    def __init__(self, deletion_time, sensor, logger):
        super().__init__()
        # Set pyplot options:
        font = {'family': 'DejaVu Sans',
                'weight': 'normal',
                'size': 15}
        plt.rc('font', **font)
        self.old_plots = deletion_time
        self.sensor = sensor
        self.old_plot = datetime.now().strftime("%m/%d/%Y, %H:%M:%S")
        self.plotted = False
        self.logger = logger


    def print_time(self, current_date, temp, hum, pres):
        self.logger.info("It is now: " + current_date.strftime("%H:%M:%S"))
        self.logger.info(f"Adding single data point: Temperature: {str(round(temp, 1))} / Humidity: {str(round(hum, 1))} / Pressure: {str(round(pres, 1))}")


    def add_average_points(self, current_date):

        print("Averaging data points.")

        # Average last half hour of data:
        try:
            avg_temperature = sum(self.tmp_temp_data) / len(self.tmp_temp_data)
            avg_humidity = sum(self.tmp_hum_data) / len(self.tmp_hum_data)
            avg_pressure = sum(self.tmp_pres_data) / len(self.tmp_pres_data)
        except:
            avg_temperature = 0
            avg_humidity = 0
            avg_pressure = 0

        # Helping error detection:
        if avg_humidity >= 100.0:
            self.logger.info("Humidity over 100%!")
            self.logger.info("Involved list:")
            for p in self.tmp_hum_data:
                print("{}  ".format(p))
            self.logger.info(f"Calculating {sum(self.tmp_hum_data)} / {len(self.tmp_hum_data)}")

        # Put data with offset for 6:00 (-12 half_hours with wrap around to 48)
        index = (round((current_date.hour * 60 + current_date.minute) / 30) - 12) % 48
        self.avg_temp_data[index] = round(avg_temperature, 1)
        self.avg_hum_data[index] = round(avg_humidity, 1)
        self.avg_pres_data[index] = round(avg_pressure, 1)

        # Empty lists:
        self.tmp_temp_data = []
        self.tmp_hum_data = []
        self.tmp_pres_data = []

        self.logger.info(f"Avg Temperature: {str(self.avg_temp_data[index])} / Humidity : {str(self.avg_hum_data[index])} / Pressure: {str(self.avg_pres_data[index])}")


    def save_plot(self, current_date):
        self.logger.info("Plotting at 6:00...")
        self.create_pyplot(current_date)  # Create new plot image
        self.avg_temp_data = [0] * 48
        self.avg_hum_data = [0] * 48
        self.avg_pres_data = [0] * 48
        self.delete_plot(self.old_plots)  # Delete old table


    def create_pyplot(self, current_date):
        fig, host = plt.subplots(figsize=(42, 8))

        par1 = host.twinx()
        par2 = host.twinx()

        # host.set_xlim(0, 2)
        host.set_ylim(ymin=0, ymax=70)
        par1.set_ylim(ymin=0, ymax=70)
        par2.set_ylim(ymin=800, ymax=max(self.avg_pres_data) + 1100)

        host.set_xlabel("Hours")
        host.set_ylabel("Temperature")
        par1.set_ylabel("Humidity")
        par2.set_ylabel("Pressure")

        host.margins(0.025)
        plt.xticks(self.time_points, self.time_labels, rotation=45)

        p1, = host.plot(self.avg_temp_data, 'ro-')
        p2, = par1.plot(self.avg_hum_data, 'bo-')
        p3, = par2.plot(self.avg_pres_data, 'go-')

        par2.spines['right'].set_position(('outward', 60))

        for x, y in zip(self.time_points, self.avg_temp_data):
            if y == 0:
                offset = 20
            else:
                offset = -38
            host.annotate("{:.1f}".format(y),  # this is the text
                          (x, y),  # these are the coordinates to position the label
                          textcoords="offset points",  # how to position the text
                          xytext=(-11, offset),  # distance from text to points (x,y)
                          ha='center',  # horizontal alignment can be left, right or center
                          weight='bold',
                          rotation=45,
                          color=p1.get_color())

        for x, y in zip(self.time_points, self.avg_hum_data):
            if y == 0:
                offset = 8
            else:
                offset = 8
            par1.annotate("{:.1f}".format(y),
                          (x, y),
                          textcoords="offset points",
                          xytext=(11, offset),
                          ha='center',
                          weight='bold',
                          rotation=45,
                          color=p2.get_color())

        for x, y in zip(self.time_points, self.avg_pres_data):
            if y == 0:
                offset = 20
            else:
                offset = -50
            par2.annotate("{:.1f}".format(y),
                          (x, y),
                          textcoords="offset points",
                          xytext=(-20, offset),
                          ha='center',
                          weight='bold',
                          rotation=45,
                          color=p3.get_color())

        host.yaxis.label.set_color(p1.get_color())
        par1.yaxis.label.set_color(p2.get_color())
        par2.yaxis.label.set_color(p3.get_color())

        plt.suptitle("Date of plot: \n" + self.old_plot + " until " + current_date.strftime("%m/%d/%Y, %H:%M:%S"))
        self.old_plot = current_date.strftime("%m/%d/%Y, %H:%M:%S")
        plt.savefig(self.save_dir + current_date.strftime("%m_%d_%Y-%H_%M_%S"), bbox_inches='tight')
        plt.clf()
        plt.cla()
        plt.close()


    def delete_plot(self, days_old):
        for filename in listdir(self.save_dir):
            if filename.endswith(".png"):
                cutoff = datetime.utcnow() - timedelta(days=days_old)
                mtime = datetime.utcfromtimestamp(path.getmtime(self.save_dir + filename))
                if mtime < cutoff:
                    # Is older than 30 days:
                    remove(self.save_dir + filename)


    def run(self):

        # Wait for init:
        time.sleep(60)

        while (not self.aborted):

            # Get time and data
            current_date = datetime.now()
            hum, temp, pres, lux = self.sensor.get_values()

            self.print_time(current_date, temp, hum, pres)

            # Add data to the list
            self.tmp_temp_data.append(temp)
            self.tmp_pres_data.append(pres)

            if hum >= 100.0:
                self.logger.info("Spurious humidity value, ignoring...")
            else:
                self.tmp_hum_data.append(hum)

            # Average values of last half hour and append to data set at correct time:
            if current_date.minute == 0 or current_date.minute == 30:

                self.add_average_points(current_date)

                # If end of day (last time point written), save plot:
                if current_date.hour == 5 and current_date.minute == 30:
                    self.create_pyplot(current_date)
                    self.plotted = True     # Remember that we plotted timely

            # We didn't plot at 6:00, so do it now:
            if not self.plotted and current_date.hour == 5 and current_date.minute == 31:
                self.create_pyplot(current_date)
                self.plotted = True

            # Reset, we definitely have plotted now:
            if self.plotted and current_date.hour == 7:
                self.plotted = False

            # Only add data point every minute:
            time.sleep(60)