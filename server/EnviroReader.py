from bme280 import BME280
import adafruit_dht
import board
import time
from threading import Thread

try:
    # Transitional fix for breaking change in LTR559
    from ltr559 import LTR559
    ltr559 = LTR559()
except ImportError:
    import ltr559
try:
    from smbus2 import SMBus
except ImportError:
    from smbus import SMBus

class EnviroReader(Thread):

    # bme280
    # dht22
    # humidity
    # temperature
    # pressure
    # aborted
    # updatesec

    def __init__(self, updatesec=5):
        super().__init__()

        self.bus = SMBus(1)
        self.bme280 = BME280(i2c_dev=self.bus)
        self.dht22 = adafruit_dht.DHT22(board.D4, use_pulseio=False)
        self.ltr559 = ltr559
        self.aborted = False
        self.updatesec = updatesec
        self.temperature, self.pressure, self.humidity, self.lux  = 0.0, 0.0, 0.0, 0.0

    def run(self):

        time.sleep(10)

        updated = False

        while (not self.aborted):

            # Get around the invalid checksum:
            while(not updated):
                try:
                    self.humidity = self.dht22.humidity
                    self.temperature = self.dht22.temperature
                    updated = True
                except:
                    continue

            updated = False

            self.pressure = self.bme280.get_pressure()
            self.lux = self.ltr559.get_lux()
            time.sleep(self.updatesec)

    def abort(self):
        self.aborted = True

    def get_values(self) -> (float, float, float, float):
        return self.humidity, self.temperature, self.pressure, self.lux

    def get_proximity(self) -> (float):
        return self.ltr559.get_proximity()

    def get_values_formatted(self) -> (str, str, str):
        return "{0:0.1f}%".format(self.humidity), "{0:0.1f} °C".format(self.temperature), "{0:0.1f} mBar".format(self.pressure)
