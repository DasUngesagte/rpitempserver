import socket
import time

HOST = 'envirozero.local'  # The server's hostname or IP address
PORT = 8080  # The port used by the server
MSGLENGTH = 16  # Maximum byte length of message


class Client():

    # humidity
    # temperature
    # pressure

    def __init__(self ):
        self.aborted = False
        self.socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        tries = 10
        while tries > 0:
            try:
                self.socket.connect((HOST, PORT))
                break
            except:
                tries -= 1
                print("Connection Error, tries left: {}\r".format(tries))
                time.sleep(10)

    def get_data(self) -> (float, float):
        self.socket.sendall("update".encode())
        humidity = self.socket.recv(MSGLENGTH).decode()
        self.socket.sendall("continue".encode())
        temperature = self.socket.recv(MSGLENGTH).decode()
        self.socket.sendall("continue".encode())
        pressure = self.socket.recv(MSGLENGTH).decode()
        self.socket.sendall("disconnect".encode())
        self.socket.shutdown(socket.SHUT_RDWR)
        self.socket.close()
        return float(humidity), float(temperature), float(pressure)


if __name__ == "__main__":
    try:
        client = Client()
        humidity, temperature, pressure = client.get_data()
        print("{0:0.1f}°C  {1:0.1f}%  {2:0.1f} mBar\r".format(temperature, humidity, pressure), flush=True)
    except:
        print("N/A°C N/A%", flush=True)
