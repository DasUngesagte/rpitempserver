import socket
import time
from threading import Thread

UPDATESEC = 10
HOST = 'envirozero.local'  # The server's hostname or IP address
PORT = 8080  # The port used by the server
MSGLENGTH = 16  # Maximum byte length of message


class Client(Thread):

    # updatesec
    # humidity
    # temperature
    # aborted

    def __init__(self, updatesec):
        super().__init__()
        self.updatesec = updatesec
        self.aborted = False
        self.humidity = 0
        self.temperature = 0

    def run(self):
        self.socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        tries = 10
        while tries > 0:
            try:
                self.socket.connect((HOST, PORT))
                break
            except:
                tries -= 1
                print("Connection Error, tries left: {}\r".format(tries))
                time.sleep(10)

        while not self.aborted:
            self.update_data()
            time.sleep(self.updatesec)

        self.socket.sendall("disconnect".encode())
        self.socket.shutdown(socket.SHUT_RDWR)
        self.socket.close()

    def update_data(self):
        self.socket.sendall("update".encode())
        humidity = self.socket.recv(MSGLENGTH).decode()
        temperature = self.socket.recv(MSGLENGTH).decode()
        self.humidity, self.temperature = float(humidity), float(temperature)

    def abort(self):
        self.aborted = True


if __name__ == "__main__":
    client = Client(UPDATESEC)
    client.start()
    time.sleep(10)

    while True:
        humidity, temperature = client.humidity, client.temperature
        print("{0:0.1f}°C {1:0.1f}%\r".format(temperature, humidity), flush=True)
        time.sleep(UPDATESEC)
